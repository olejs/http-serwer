#include "serwer.hpp"
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <thread>
#include <vector>
  
#define my_port "8090"
#define MAXEVENTS 64
     
using namespace std;
     
void worker(int ifd);
     
class SharedData {
public:
    SharedData() :
        connection_threads() {
           
    }
    void create_connection_thread(int socket) {
        thread t(worker, socket);
        connection_threads.push_back(std::move(t));
    }
       
private:
    vector<thread> connection_threads;
};

class ProcessHeader {
public:
    ProcessHeader() :
            header(), body(){
        this->isloaded = false;
        len = 0;
        memset(&buf[0],0,sizeof(buf));
        is_content_length = false;
        readed_sum = 0;
    }

    ssize_t read_buf(int fd){
        bool isnheader;
        ssize_t readed;
        size_t offset, pos;
        char  *eol, *bol;

        isnheader = this->isloaded;
        len = (size_t)0;


        // len is member of this class, so the offset stays same even for different epoll reads.
        while((readed = read(fd, &buf[len], (1023-len))) > 0) {
            readed_sum += readed;


            //appending body of the buffer
            if (isnheader) {

                this->body.append(buf, readed);
        }

            // process headers
            if (!isnheader) {

                // calculate combined length of unprocessed data and new data
                len += readed;

                // NULL terminate buffer for string functions
                buf[len] = '\0';

                // checks if the header break happened to be the first line of the
                // buffer
                if (!(strncmp(buf, "\r\n", 2))) {

                    if (len > 2){
                        this->body.append(buf,len-2);}
                        //fwrite(buffer, 1, (len - 2), fp);
                    continue;
                };
                if (!(strncmp(buf, "\n", 1))) {
                    if (len > 1){
                        this->body.append(buf,len-1);}
                        //fwrite(buffer, 1, (len - 1), fp);
                    continue;
                };

                //checks if Content-Length field happened to be this line
                if(!(strncmp(buf,"Content-Length:",15))){
                    if (len > 15){
                        eol = index(buf, '\n');
                        if( eol != NULL){
                            char number[10];
                            strncpy(number,buf+16,eol-buf-16);
                            content_length = atoi(number);
                            cout<<content_length<"\n";
                            is_content_length = true;
                        }
                    }
                }

                // process each line in buffer looking for header break
                bol = buf;  // bol = break of line
                while ((eol = index(bol, '\n')) != NULL) {

                    // There was no header break, so I sincerely append the line to the header
                    if(!isnheader)
                        this->header.append(bol,eol-bol+1);
                    // update bol based upon the value of eol
                    bol = eol + 1;

                    if(!(strncmp(buf,"Content-Length:",15))){
                        if (len > 15){
                            char* s = index(buf, '\n');
                            if( s != NULL){
                                char number[10];
                                strncpy(number,buf+16,s-buf-16);
                                content_length = atoi(number);
                                cout<<content_length<"\n";
                                is_content_length = true;
                            }
                        }
                    }

                    // test if end of headers has been reached
                    if ((!(strncmp(bol, "\r\n", 2))) || (!(strncmp(bol, "\n", 1)))) {

                        // note that end of headers has been reached
                        isnheader = true;
                        this->isloaded = true;

                        // update the value of bol to reflect the beginning of the line
                        // immediately after the headers
                        if (bol[0] != '\n')
                            bol += 1;
                        bol += 1;

                        // calculate the amount of data remaining in the buffer
                        len = len - (bol - buf);

                        //if((bol-buf) > 0)
                         //   this->header.append()
                        // write remaining data to FILE stream
                        if (len > 0) {
                            this->body.append(bol, len);
                        }
                            //fwrite(bol, 1, len, fp);

                        // reset length of left over data to zero and continue processing
                        // non-header information
                        len = 0;
                    }
                }

                if (!isnheader) {
                    // shift data remaining in buffer to beginning of buffer
                    offset = (bol - buf);
                    for (pos = 0; pos < offset; pos++)
                        buf[pos] = buf[offset + pos];

                    // save amount of unprocessed data remaining in buffer
                    len = offset;
                }
            }
        }
        return readed_sum;
    }

    void print(){
        cout<<"To Jest Naglowek\n";
        cout<<this->header<<endl;
    }
    string get_body(){
        return this->body;
    }
    string get_header(){
        return this->header;
    }
    bool get_isloaded(){
        return isloaded;
    }
    bool get_is_content_length(){
        return is_content_length;
    }

    int to_read(){
        return content_length - readed_sum;
    }

    ssize_t readed_sum;
private:
    string header;
    string body;
    bool isloaded;
    char buf[1024];
    size_t len;
    int content_length;
    bool is_content_length;
};

static int
make_socket_non_blocking(int sfd) {
    int flags, s;
     
    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }
     
    flags |= O_NONBLOCK;
    s = fcntl(sfd, F_SETFL, flags);
    if (s == -1) {
        perror("fcntl");
        return -1;
    }
     
    return 0;
}
     
static int
create_and_bind(char *port) {
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s, sfd;
     
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;     /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* We want a TCP socket */
    hints.ai_flags = AI_PASSIVE;     /* All interfaces */

    s = getaddrinfo(NULL, port, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }
     
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;
     
        s = bind(sfd, rp->ai_addr, rp->ai_addrlen);
        if (s == 0) {
            break;
        }
     
        close(sfd);
    }
     
    if (rp == NULL) {
        fprintf(stderr, "Could not bind\n");
        return -1;
    }
     
    freeaddrinfo(result);
     
    return sfd;
}
     
void error(const char *msg) {
    perror(msg);
    exit(1);
}
    
void worker(int ifd) {
    int efd;
    efd = epoll_create1(0);
       
    struct epoll_event event;
    event.data.fd = ifd;
    event.events = EPOLLIN | EPOLLET;
     
    int s = epoll_ctl(efd, EPOLL_CTL_ADD, ifd, &event);

    ProcessHeader header; // class responsible for reading headers.

    struct epoll_event *events;
    events = static_cast<epoll_event *>(calloc(MAXEVENTS, sizeof(struct epoll_event)));
       
    if (s == -1) {
        perror("epoll_ctl");
        abort();
    }
       
    while (1) {
        int n = epoll_wait(efd, events, MAXEVENTS, -1);
           
        if (n == 0) {
            continue;
        } else if (n == -1) {
            abort();
        }
           
        for (int i = 0; i < n; i++) {
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP)) {
                return;
            } else if ((events[i].events & EPOLLIN)) {
                ssize_t count;
                //char buf[1024];
                //count = read(events[i].data.fd, buf, sizeof(buf));
                //perror(buf);

                if(!header.get_isloaded() || (header.get_is_content_length() && header.to_read()>0 )) {
                    count = header.read_buf(events[i].data.fd);
                    header.print();
                    //cout<<"\n\n"<<header.get_body()<<endl;
                    if (count == -1) {
                        if (errno != EAGAIN) {
                            perror("read");
                            return;
                        }
                    }
                }
                   
                s = write(events[i].data.fd, "HTTP/1.0 200 OK\n", 16); // Write back to client. to bound socket from client. they see this shit.
                s = write(events[i].data.fd, "Connection: Keep-alive\n", 23);
                s = write(events[i].data.fd, "Connection: close\n", 18);
                s = write(events[i].data.fd, "Content-type: text/html; charset=UTF-8\n", 39);
                s = write(events[i].data.fd, "\n", 1);
                s = write(events[i].data.fd, "<!DOCTYPE HTML>\n<html><head><title>Siemka luju</title></head>\n", 62);
                s = write(events[i].data.fd, "<body><h1>Hello World!</h1>\n</body></html>\n", 28);
                s = write(events[i].data.fd, "</body></html>\n", 15);
                   
                if (s == -1) {
                    perror("write");
                    abort();
                }
                   
                close(events[i].data.fd);
            }
        }
    }
}
     
void task(int n, epoll_event *events, int sfd, int efd, SharedData *data) {
    int i, s;
     
    for (i = 0; i < n; i++) {
        if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN))) {
            perror("epoll error");
            continue;
        } else if (sfd == events[i].data.fd) {
            while (1) {
                struct sockaddr in_addr;
                socklen_t in_len;
                int infd;
                char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
    
                in_len = sizeof in_addr;
                infd = accept(sfd, &in_addr, &in_len);
    
                if (infd == -1) {
                    if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                        break;
                    } else {
                        perror("accept");
                        break;
                    }
                }
                s = getnameinfo(&in_addr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf, NI_NUMERICHOST | NI_NUMERICSERV);
                if (s == 0) {
                    printf("Accepted connection on descriptor %d (host=%s, port=%s)\n", infd, hbuf, sbuf);
                }
                s = make_socket_non_blocking(infd);
                if (s == -1)
                    abort();
                   
                data->create_connection_thread(infd);
            }
            continue;
        }
    }
}
     
int main(int argc, char *argv[]) {
    int sfd, s;
    int efd;
    struct epoll_event *events;

    sfd = create_and_bind((char *)my_port);
    s = make_socket_non_blocking(sfd);
    s = listen(sfd, SOMAXCONN);

    efd = epoll_create1(0);
     
    struct epoll_event event;
    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLET;

    s = epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);
    events = static_cast<epoll_event *>(calloc(MAXEVENTS, sizeof(struct epoll_event)));
     
    SharedData *data = new SharedData();
     
    while (1) {
        int n;

        n = epoll_wait(efd, events, MAXEVENTS, -1);
           
        if (n <= -1) {
            break;
        } else if (n == 0) {
            continue;
        } else {
            task(n, events, sfd, efd, data);
        }
    }
       
    free(events);
    close(sfd);
     
    return EXIT_SUCCESS;
}

